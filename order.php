<?php include("views/partials/loader.php");


if($_POST && $isLogged) {
	$item = new Item;
	$order = new Order;

	$record = $item->find($_POST['id']);

	$orderId = $order->set([
		'customer_id' => $customerId,
		'amount' => $record->price,
		'created_at' => date('Y-m-d H:i:s')
	]);

	$itemOrder = new ItemOrder;
	$itemOrder->set($orderId, $record->id);

}

