<?php

class ItemOrder extends DB 
{

	protected $table = "item_orders";

	public function __construct() 
	{
		parent::__construct();
	}

	public function all() 
	{
		$query = $this->connection->prepare("SELECT 
			$this->table.id as order_id,
			customers.name as customer_name,
			orders.amount as order_amount,
			DATE_FORMAT(orders.created_at, '%W, %M %e %Y at %l:%i %p') as order_at,
			items.name as item_name,
			items.description as item_description
				FROM $this->table 
			LEFT JOIN orders ON 
				$this->table.order_id = orders.id 
			LEFT JOIN customers ON 
				orders.customer_id = customers.id 
			LEFT JOIN items ON 
			$this->table.item_id = items.id ORDER BY order_id DESC");
		$query->execute();

		$result = $query->setFetchMode(PDO::FETCH_OBJ); 

		return $query->fetchAll();
	}

	public function find($id) 
	{
		$stmt = $this->connection->prepare("SELECT * FROM $this->table WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_INT); 
		$stmt->execute();

		$result = $stmt->setFetchMode(PDO::FETCH_OBJ); 

		return $stmt->fetch();
	}

	public function set($orderId, $itemId) 
	{
		return $this->connection->exec($this->generateInsertSql($this->table, [
			'order_id' => $orderId,
			'item_id' => $itemId
		]));
	}

}