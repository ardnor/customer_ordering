<?php

class Customer extends DB 
{

	public function __construct() 
	{
		parent::__construct();
	}

	public function all() 
	{
		$query = $this->connection->prepare("SELECT * FROM customers");
		$query->execute();

		$result = $query->setFetchMode(PDO::FETCH_OBJ); 

		return $query->fetchAll();
	}

}