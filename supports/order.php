<?php

class Order extends DB 
{

	protected $table = "orders";

	public function __construct() 
	{
		parent::__construct();
	}

	public function all() 
	{
		$query = $this->connection->prepare("SELECT * FROM " . $this->table);
		$query->execute();

		$result = $query->setFetchMode(PDO::FETCH_OBJ); 

		return $query->fetchAll();
	}

	public function set(array $attributes) 
	{
	   	$this->connection->exec($this->generateInsertSql($this->table, $attributes));
	   	return $this->connection->lastInsertId();
	}

}