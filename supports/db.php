<?php

abstract class DB {

	protected $host;

	protected $dbname;

	protected $user;

	protected $pass;

	protected $connection;

	public function __construct() 
	{
		$this->host = HOST;
		$this->dbname = DBNAME;
		$this->user = USER;
		$this->pass = PASS;

		$this->connect();
	}

	protected function connect() 
	{
		try {
			$this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {
			echo $e->getMessage();
		}

		return $this;
	}

	public function generateInsertSql($table, $attributes) 
	{
		$sql = "INSERT INTO $table  (";

		$cntr = 1;
		foreach($attributes as $column=>$value) {
			$comma = ($cntr<count($attributes))?", ":"";
			$sql .= "$column$comma";

			$cntr++;
		}
		$sql .= ") VALUES (";

		$cntr = 1;
		foreach($attributes as $column=>$value) {
			$comma = ($cntr<count($attributes))?", ":"";
			$sql .= "'$value'$comma";

			$cntr++;
		}
		$sql .= ")";

		return $sql;
	}
}