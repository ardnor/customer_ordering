<?php

if(isset($_GET['sessionId'])) {
	$sessionID = $_GET['sessionId'];

	setcookie('customer_id', $sessionID, time() + (86400 * 30), "/");
}

if(isset($_GET['logout']) && $_GET['logout'] == 1 && isset($_COOKIE['customer_id'])) {
	unset($_COOKIE['customer_id']);
    unset($_COOKIE['customer_id']);
    setcookie('customer_id', null, -1, '/');
    setcookie('customer_id', null, -1, '/');
}

$isLogged = isset($_COOKIE['customer_id']);

$customerId = ($isLogged)?$_COOKIE['customer_id']:null;