<?php

class Item extends DB 
{

	protected $table = "items";

	public function __construct() 
	{
		parent::__construct();
	}

	public function all() 
	{
		$query = $this->connection->prepare("SELECT * FROM $this->table");
		$query->execute();

		$result = $query->setFetchMode(PDO::FETCH_OBJ); 

		return $query->fetchAll();
	}

	public function find($id) 
	{
		$stmt = $this->connection->prepare("SELECT * FROM $this->table WHERE id = :id");
		$stmt->bindParam(':id', $id, PDO::PARAM_INT); 
		$stmt->execute();

		$result = $stmt->setFetchMode(PDO::FETCH_OBJ); 

		return $stmt->fetch();
	}

}