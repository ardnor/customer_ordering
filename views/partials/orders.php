
<?php
	$itemOrders = new ItemOrder;
?>

<table class="table table-striped table-hover table-bordered">
  <thead>
    <tr>
    	<th scope="col">#</th>
    	<th scope="col">Order</th>
      	<th scope="col">Customer Name</th>
      	<th scope="col">Amount</th>
      	<th scope="col">Date Ordered</th>
    </tr>
  </thead>
  <tbody>
  	<?php foreach($itemOrders->all() as $order):?>
    <tr>
    	<td><?=$order->order_id?></td>
    	<td>
    		<b><?=$order->item_name?></b>
    		<br>
    		<span><?=$order->item_description?></span>	
    	</td>
      	<td><?=$order->customer_name?></td>
      	<td><?=$order->order_amount?></td>
      	<td><?=$order->order_at?></td>
    </tr>
	<?php endforeach;?>
  </tbody>
</table>