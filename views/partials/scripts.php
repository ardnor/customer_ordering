<script type="text/javascript">
	$(document).ready(function() {

		<?php if(isset($_GET['sessionId'])): ?>
			window.location="<?=PROJECT_URL;?>";
		<?php endif; ?>

		$("#log-as-user").on("change", function() {
			window.location=$(this).val();
			
		});

		$(".btnOrderNow").on("click", function(){
			console.log(11)
			var $this = $(this);
			var id = $this.data('id');
			$.ajax({
		        type: "POST",
		        url: "order.php",
		        data: {id:id},
		        success: function (response) {
		        	window.location="<?=PROJECT_URL.'orders.php';?>";
		        }
		    });
		});
	});
</script>