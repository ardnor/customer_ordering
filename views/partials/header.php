<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
  <h5 class="my-0 mr-md-auto font-weight-normal">Company name</h5>
  <nav class="my-2 my-md-0 mr-md-3">

    <?php

      $customer = new Customer();
    ?>

    <a class="p-2 text-dark" href="<?=PROJECT_URL;?>orders.php">Orders</a>

    <span class="p-2 text-dark" href="javascript:void;">
      <?php if($isLogged): ?>
        Logged as
      <?php else:?>
        Log as
      <?php endif;?>
      
      <select id="log-as-user">
      <?php foreach ($customer->all() as $customer) : ?>
        <option value="<?=PROJECT_URL;?>?sessionId=<?=$customer->id;?>" <?php echo $customerId==$customer->id?'selected':'';?>><?=$customer->name;?></option>
      <?php endforeach; ?>
      </select>
      <?php if($isLogged): ?>
        <a class="p-2 text-dark" href="<?=PROJECT_URL;?>?logout=1">Logout</a>
      <?php endif;?>
    </span>

    
  </nav>
</div>