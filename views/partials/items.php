<?php
  $items = new Item();
?>

<div class="card-deck mb-3 text-center">

  <?php foreach ($items->all() as $item) : ?>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal"><?=$item->name;?></h4>
      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title"><?=$item->price;?></h1>
        <p><?=$item->description;?></p>
        <button type="button" class="btn btn-lg btn-block btn-primary btnOrderNow" data-id="<?=$item->id;?>"  <?=(! $isLogged) ? "disabled" : ""; ?>>Order Now</button>
      </div>
    </div>
  <?php endforeach; ?>
</div>